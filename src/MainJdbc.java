import cn.com.do4u.model.UserInfo;
import cn.com.do4u.model._MappingKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.IDataSourceProvider;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.druid.DruidPlugin;

import java.util.List;

/**
 * Created by chenyu on 2016/3/9.
 */
public class MainJdbc {

    public static C3p0Plugin createC3p0Plugin() {
        PropKit.use("a_little_config.txt");
        C3p0Plugin dp = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
        dp.start();
        return dp;
    }

    public static DruidPlugin createDruidPlugin() {
        PropKit.use("a_little_config.txt");
        DruidPlugin dp = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
        dp.setInitialSize(3);
        dp.setMinIdle(2);
        dp.setMaxActive(5);
        dp.setMaxWait(60000);
        dp.setTimeBetweenEvictionRunsMillis(120000);
        dp.setMinEvictableIdleTimeMillis(120000);
        dp.start();
        return  dp;
    }

    public static void initDruid(){
        // 配置Druid数据库连接池插件
        initActiveRecord(createDruidPlugin());
    }

    public static void initC3p0(){
        // 配置C3p0数据库连接池插件
        initActiveRecord(createC3p0Plugin());
    }

    public static void initActiveRecord(IDataSourceProvider dp){
        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        arp.setDevMode(true);
        arp.setShowSql(true); //是否打印sql语句
        //映射数据库的表和继承与model的实体
        //只有做完该映射后，才能进行junit测试
        _MappingKit.mapping(arp);
        arp.start();
    }



    public static void main(String[] args) {
//        initC3p0();
        initDruid();
        List<UserInfo> admins = UserInfo.dao.find("select * from user_info");
        System.out.println(admins.size());
        System.out.println(admins);
    }

}
