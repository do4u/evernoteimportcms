import cn.techjc.EvernoteUtil;
import com.evernote.edam.notestore.NoteList;
import com.evernote.edam.type.Data;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.techjc.FileUtil.bytes2file;
import static cn.techjc.FileUtil.string2file;
import static cn.techjc.FileUtil.MD5;
import static cn.techjc.FileUtil.pattern;
import static cn.techjc.FileUtil.patternOne;

/**
 * Created by chenyu on 2016/1/10.
 */
public class MainClass {

    public static void main(String[] args) throws Exception {
        EvernoteUtil eveynote = new EvernoteUtil();

        NoteList list= eveynote.getNotesByBid("23a636be-1207-4e39-be3b-40e2f41537ce",0,100);

        List<Note> listNote = list.getNotes();

        String reg0 = "(?<=<en-note>).{1,}(?=</en-note>)";
        String reg1 = "<en-media hash=\"\\w{32}\" style=\".{7,20}\" type=\".{9,30}\"/>";
        String reg2 = "<en-media ";//<img
        String reg3 = "(?<=hash=\")\\w{32}(?=\")";
        String reg4 = "(?<=/)gif|jpeg|png|wav|mpeg|amr|pdf|vnd.evernote.ink(?=\"/)";

        for (Note note : listNote){
            String guid = note.getGuid();

            note = eveynote.getNote(guid);
            String content = note.getContent();

            //将content替换成html标签
            content = patternOne(reg0,content);
            content = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>"+note.getTitle()+"</title></head><body>"+content+"</body></html>";
            //md5命名的文件名及其数组对象
            Map<String,byte[]> fileFullNameMap = new HashMap<String, byte[]>();

            //需要替换的标签
            Map<String,String> hyperMap = new HashMap<String, String>();

            List<String> listStr = pattern(reg1,content);
            for(String media : listStr){
                String img = patternOne(reg2,media);
                String str = media.replace(img ,"<img ");
                str = str.replace("hash" ,"src");
                String fileName = patternOne(reg3,media);
                String dotName = patternOne(reg4,media);
                str = str.replace(fileName ,"image/"+fileName+"."+dotName);
                hyperMap.put(media,str);
            }

            List<Resource> resources = note.getResources();
            if (resources != null && resources.size() > 0) {
                for (Resource resource : resources) {
                    Data data = resource.getData();
                    String dotName = resource.getMime().split("/")[1];//获取文件后缀名
                    byte[] bytes = data.getBody();
                    String md5Str = MD5(bytes);
                    String fileFullName = md5Str + "." + dotName;
                    fileFullNameMap.put(fileFullName, bytes);
                }
            }

            for(Map.Entry<String,String> entry:hyperMap.entrySet()){
                content = content.replace(entry.getKey(),entry.getValue());
            }
            string2file(content,guid+".html");
            //将附件写成文件
            for(Map.Entry<String ,byte[]> entry : fileFullNameMap.entrySet()){
                bytes2file(entry.getValue(),entry.getKey());
            }
            System.out.println(note.getTitle());
            System.out.println(note.getGuid());
            System.out.println(note.getContent());
        }
    }

}
